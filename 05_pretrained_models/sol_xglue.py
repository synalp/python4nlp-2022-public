
cats = {
        "sports":1,
        "travel":2,
        "finance":3,
        "lifestyle":4,
        "news":5,
        "entertainment":6,
        "health":7,
        "video":8,
        "autos":9,
        }

def init():
    import transformers
    from datasets import list_datasets, load_dataset
    dataset = load_dataset('xglue','nc',split='validation.fr')
    print(dataset)
    for x in dataset:
        print(x['news_body'],x['news_category'])

def run():
    import datasets
    import urllib.parse
    import requests

    with open("xglue.20.txt","r",encoding="utf-8") as f:
        nok,ntot = 0,0
        for l in f:
            l=l.strip()
            txt=l[0:-1].replace('/',' ')
            c=int(l[-1:])

            s = '"'+txt+'", given a list of categories: "sports, travel, finance, lifestyle, news, entertainment, health, video or autos", what category does the paragraph belong to?'
            print(c,txt)
            st = urllib.parse.quote_plus(s)
            r = requests.get('http://fb.cerisara.fr/t0pp/'+st)
            rr = r.text.lower()
            print(rr)
            pred=-1
            for k,v in cats.items():
                if k in rr:
                    pred=v
                    break
            if pred==c: nok+=1
            ntot+=1
            acc=float(nok)/float(ntot)
            print("acc",acc)

# init()
run()


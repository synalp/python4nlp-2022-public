import datasets
import urllib.parse
import requests
d=datasets.load_dataset("Jikiwa/glue-mnli-train")['validation']
# 0=infer 1=neutral 2=contredict

def testT0pp():
    nok,ntot = 0,0
    for i in range(10):
      h = d[i]['hypothesis']
      p = d[i]['premise']
      label = int(d[i]['label'])
      s = p+'\n'+'Based on the previous passage, is it true that "'+h+'"? Yes, no, or maybe?'
      print(s)
      st = urllib.parse.quote_plus(s)
      r = requests.get('http://fb.cerisara.fr/t0pp/'+st)
      rr = r.text.lower()
      if 'yes' in rr: rep=0
      elif 'no' in rr: rep=2
      elif 'maybe' in rr: rep=1
      else: rep=-1
      if rep<0:
          print("ERROR",rr)
      else:
          if rep==label: nok+=1
          ntot+=1
          acc=float(nok)/float(ntot)
          print("acc",acc)

def testBARTMNLI():
    from transformers import AutoModelForSequenceClassification, AutoTokenizer
    nli_model = AutoModelForSequenceClassification.from_pretrained('facebook/bart-large-mnli')
    tokenizer = AutoTokenizer.from_pretrained('facebook/bart-large-mnli')

    nok,ntot = 0,0
    for i in range(10):
        h = d[i]['hypothesis']
        p = d[i]['premise']
        label = int(d[i]['label'])
        print(p," || ",h," || ",label)

        x = tokenizer.encode(p, h, return_tensors='pt', truncation_strategy='only_first')
        logits = nli_model(x)[0]
        score_infer = logits[0,2].item()
        score_neutral = logits[0,1].item()
        score_contredict = logits[0,0].item()
        print(score_infer,score_neutral,score_contredict)

        rep=-1
        if score_infer>=score_neutral and score_infer>=score_contredict: rep=0
        elif score_contredict>=score_neutral and score_contredict>=score_infer: rep=2
        elif score_neutral>=score_infer and score_neutral>=score_contredict: rep=1

        if rep<0:
          print("ERROR")
        else:
          if rep==label: nok+=1
          ntot+=1
          acc=float(nok)/float(ntot)
          print("acc",acc)

testT0pp()
# testBARTMNLI()

